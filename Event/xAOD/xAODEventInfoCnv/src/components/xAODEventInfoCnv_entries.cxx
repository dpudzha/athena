#include "../EventInfoCnvTool.h"
#include "../EventInfoSelectorTool.h"
#include "../EventInfoCnvAlg.h"
#include "../EventInfoReaderAlg.h"
#include "../EventDuplicateFinderAlg.h"

DECLARE_COMPONENT( xAODMaker::EventInfoCnvTool )
DECLARE_COMPONENT( xAODMaker::EventInfoSelectorTool )
DECLARE_COMPONENT( xAODMaker::EventInfoCnvAlg )
DECLARE_COMPONENT( xAODReader::EventInfoReaderAlg )
DECLARE_COMPONENT( xAODReader::EventDuplicateFinderAlg )
